import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly host = environment.host;

  constructor(private http: HttpClient) { }

  public get(path: string): Observable<any> {
    console.log(this.host + "/" + path + "/get/all");
    return this.http.get<any>(this.host + "/" + path + "/get/all");
  }

  public getById(path: string, id: any) : Observable<any> {
    return this.http.post<any>(this.host + '/' + path , id);
  }

  public post(path: string, body: any): Observable<any> {
    return this.http.post<any>(this.host + "/" + path + "/create", body);
  }

  public delete(path: string, body: any): Observable<any> {
    return this.http.delete<any>(this.host + "/" + path + "/delete/" + body);
  }

  public patch(path: string, body: any , id:any): Observable<any> {
    return this.http.patch<any>(this.host + "/" + path + "/" + id, body);
  }
}
