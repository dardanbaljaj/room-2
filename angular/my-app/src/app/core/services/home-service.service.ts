import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeServiceService {
  private readonly path = environment.endpoint.componentController;


  constructor(private api: ApiService) { }

  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }
  public getById(ut: any): Observable<any> {
    return this.api.getById(this.path , ut);
  }
  public add(item: any): Observable<any> {
    return this.api.post(this.path, item);
  }
  public deleteById(body: any): Observable<any> {
    return this.api.delete(this.path, body);
  }
  public update(soggetto: any , id:any): Observable<any> {
    return this.api.patch(this.path, soggetto , id);
  }
}
