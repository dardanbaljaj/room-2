import { Component, OnInit } from "@angular/core";
import { HomeServiceService } from "src/app/core/services/home-service.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.css"],
})
export class HomePageComponent implements OnInit {
  public lista: any[] = [];

  constructor(public homeServices: HomeServiceService, public router: Router) {}

  ngOnInit() {
    this.homeServices.getAll().subscribe((resp) => {
      this.lista = resp.response;
    });
  }

  remove(ut : any){
    this.homeServices.deleteById(ut.codice_fiscale).subscribe((resp)=>{
      this.homeServices.getAll().subscribe((resp) => {
        this.lista = resp.response;
      });
    });
  }

  add(){
    this.router.navigate(['/add']);
  }
}
