import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HomeServiceService } from 'src/app/core/services/home-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  public formGroup: FormGroup;
  constructor(public fb: FormBuilder, public services : HomeServiceService,  public router: Router) { }

  send: boolean = false;

  ngOnInit() {
    this.formGroup = this.fb.group({
      codice_fiscale: [''],
      nome: [''],
      cognome: [''],
      data_di_nascita: [''],
      luogo_di_nascita: [''],
      sesso: [''],
      immagine: ['']
    });
  }

conferma(){
  const values = this.formGroup.value;
  //values.immagine = this.tagliaPath(this.formGroup.controls['immagine'].value);
  this.send = false;

  let newFinale = Object.assign({}, this.formGroup);
  this.services.add(values).subscribe(() => {
    this.send = true;
    this.formGroup.reset();
    console.table(values);
  })
}

tagliaPath(img: any){
  let path: string = '';
  for (let i = 12; i <= img.length; i++) {
    path = path + img.charAt(i);
  }
  return path;
}

}
