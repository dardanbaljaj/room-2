import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { AddUserComponent } from "./pages/add-user/add-user.component";

const routes: Routes = [
  { path: "home", component: HomePageComponent},
  { path: "add", component: AddUserComponent },
  { path: "", component: AddUserComponent, pathMatch: 'full' },
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
