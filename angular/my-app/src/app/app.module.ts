import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomePageComponent } from "./pages/home-page/home-page.component"; //accordion and accordion tab
import { HttpClientModule } from "@angular/common/http";
import { DataViewModule } from "primeng/dataview";
import { AddUserComponent } from "./pages/add-user/add-user.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';  

@NgModule({
  declarations: [AppComponent, HomePageComponent, AddUserComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DataViewModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
