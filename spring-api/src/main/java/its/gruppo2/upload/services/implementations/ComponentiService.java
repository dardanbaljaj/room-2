package its.gruppo2.upload.services.implementations;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import its.gruppo2.upload.dao.ComponentiDao;
import its.gruppo2.upload.dto.ComponentiDto;
import its.gruppo2.upload.dto.ImmagineDto;
import its.gruppo2.upload.enumerations.ServiceResponse;
import its.gruppo2.upload.repository.ComponentiRepository;
import its.gruppo2.upload.services.interfaces.IComponentiService;
import net.coobird.thumbnailator.Thumbnails;

@Service
public class ComponentiService implements IComponentiService{

	@Autowired
	private ComponentiRepository componentiRepository;
	
	@Override
	public ServiceResponse insert(ComponentiDto dto) {
		if (dto == null) return ServiceResponse.EMPTY_DTO;
		try {
			ComponentiDao dao = new ComponentiDao();
			sizemage(dto.getImmagine());
			dtoToDao(dto, dao);
			componentiRepository.save(dao);
			return ServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return ServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			return ServiceResponse.CREATE_FAILED;
		}
	}
	public  File load(String url) {
		 File bit=new File("img.png");
	    try {
			URL urlo=new URL(url);
			BufferedImage image=ImageIO.read(urlo);
			FileOutputStream d=new FileOutputStream(bit);
			ImageIO.write(image,"png",d);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	    return bit;
	}
	
	@Override
    public void sizemage(String g) {
    	File f = load(g);
    	System.out.println(g);
    	System.out.println(f);
    	File out=new File("Download\\"+ (Math.random()*4) +".png");
    	try {
    		if(!out.exists())out.createNewFile();
			Thumbnails.of(f).size(200,200).toFile(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Override
	public List<ComponentiDto> getAll() {
		try {
			List<ComponentiDto> componentiDto = new ArrayList<>();
			List<ComponentiDao> componentiDao = componentiRepository.findAll();
			for (ComponentiDao dao : componentiDao) {
				ComponentiDto dto = new ComponentiDto();
				daoToDto(dao, dto);
				componentiDto.add(dto);
			}
			return componentiDto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ComponentiDto getByCodiceFiscale(String codiceFiscale) {
		try {
			ComponentiDao dao = componentiRepository.findByCodiceFiscale(codiceFiscale);
			ComponentiDto dto = new ComponentiDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public ImmagineDto getImgByCodiceFiscale(String codiceFiscale) {
		try {
			ComponentiDao dao = componentiRepository.findByCodiceFiscale(codiceFiscale);
			ImmagineDto dto = new ImmagineDto();
			dto.setImmagine(dao.getImmagine());
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public ServiceResponse update(String codiceFiscale, ComponentiDto dto) {
		if (dto == null) return ServiceResponse.EMPTY_DTO;
		if (codiceFiscale == null) return ServiceResponse.EMPTY_CODICE_FISCALE;
		try {
			System.out.println("" + dto.getNome());
			System.out.println("" + dto.getCognome());
			ComponentiDao dao = componentiRepository.findByCodiceFiscale(codiceFiscale);
			System.out.println("" + dao.getCognome());
			dtoToDao(dto, dao);
			System.out.println("" + dao.getNome());
			System.out.println("" + dao.getCognome());
			System.out.println("" + dao.getData_di_nascita());
			componentiRepository.save(dao);
			return ServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace();
			return ServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return ServiceResponse.UPDATE_FAILED;
		}
	}

	@Override
	public ServiceResponse deleteByCodiceFiscale(String codiceFiscale) {
		if (codiceFiscale == null) return ServiceResponse.EMPTY_CODICE_FISCALE;
		try {
			componentiRepository.deleteByCodiceFiscale(codiceFiscale);
			return ServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return ServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			return ServiceResponse.DELETE_FAILED;
		}
	}

	@Override
	public void daoToDto(ComponentiDao dao, ComponentiDto dto) {
		dto.setCodice_fiscale(dao.getCodiceFiscale());
		dto.setNome(dao.getNome());
		dto.setCognome(dao.getCognome());
		dto.setData_di_nascita(dao.getData_di_nascita());
		dto.setLuogo_di_nascita(dao.getLuogo_di_nascita());
		dto.setSesso(dao.getSesso());
		dto.setImmagine(dao.getImmagine());
	}

	@Override
	public void dtoToDao(ComponentiDto dto, ComponentiDao dao) {
		dao.setCodiceFiscale(dto.getCodice_fiscale());
		dao.setNome(dto.getNome());
		dao.setCognome(dto.getCognome());
		dao.setData_di_nascita(dto.getData_di_nascita());
		dao.setLuogo_di_nascita(dto.getLuogo_di_nascita());
		dao.setSesso(dto.getSesso());
		dao.setImmagine(dto.getImmagine());
	}
	
}
