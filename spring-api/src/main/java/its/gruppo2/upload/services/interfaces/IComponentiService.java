package its.gruppo2.upload.services.interfaces;

import java.util.List;

import its.gruppo2.upload.dao.ComponentiDao;
import its.gruppo2.upload.dto.ComponentiDto;
import its.gruppo2.upload.dto.ImmagineDto;
import its.gruppo2.upload.enumerations.ServiceResponse;

public interface IComponentiService {

	// insert
	ServiceResponse insert(ComponentiDto dto);
	
	// get all
	List<ComponentiDto> getAll();
	
	// get by codiceFiscale
	ComponentiDto getByCodiceFiscale(String codiceFiscale);
	
	// get img by Codice Fiscale
	ImmagineDto getImgByCodiceFiscale(String codiceFiscale);
	
	// update
	ServiceResponse update(String codiceFiscale, ComponentiDto dto);
	
	// delete
	ServiceResponse deleteByCodiceFiscale(String codiceFiscale);
	
	// dao to dto
	void daoToDto(ComponentiDao dao, ComponentiDto dto);
	
	// dto to dao
	void dtoToDao(ComponentiDto dto, ComponentiDao dao);
	
	public void sizemage(String g);
	
}
