package its.gruppo2.upload.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComponentiDto {

	@NotNull
	private String codice_fiscale;
	
	private String nome;
	
	private String cognome;
	
	private Date data_di_nascita;
	
	private String luogo_di_nascita;
	
	private String sesso;
	
	private String immagine;
	
}
