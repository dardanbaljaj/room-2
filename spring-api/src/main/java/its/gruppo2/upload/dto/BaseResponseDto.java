package its.gruppo2.upload.dto;

import java.util.Date;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BaseResponseDto<T> {

	private Date timestamp = new Date();
	
	private int status = HttpStatus.OK.value();
	
	private boolean success = true;
	
	private boolean error = false;
	
	private T response;
	
}
