package its.gruppo2.upload.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import its.gruppo2.upload.dto.BaseResponseDto;
import its.gruppo2.upload.dto.ComponentiDto;
import its.gruppo2.upload.dto.ImmagineDto;
import its.gruppo2.upload.enumerations.ServiceResponse;
import its.gruppo2.upload.services.implementations.ComponentiService;

@RestController
@RequestMapping(value = "api/components")
public class ComponentiController {
	@Autowired
	private ComponentiService componentiService;
	
	@PostMapping(value = "/create")
	private BaseResponseDto<ServiceResponse> insert(@RequestBody(required = false) ComponentiDto dto) {
		BaseResponseDto<ServiceResponse> result = new BaseResponseDto<>();
		ServiceResponse serviceStatus = componentiService.insert(dto);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	@GetMapping(value = "/get/all", produces = "application/json")
	private BaseResponseDto<List<ComponentiDto>> getAll() {
		BaseResponseDto<List<ComponentiDto>> result = new BaseResponseDto<>();
		result.setResponse(componentiService.getAll());
		return result;
	}
	@GetMapping(value = "/get/size/{path}", produces = "application/json")
	private BaseResponseDto<ImmagineDto> getImgByPath(@PathVariable("path") String path) {
		BaseResponseDto<ImmagineDto> result = new BaseResponseDto<>();
	     componentiService.sizemage(path);
		return result;
	}
	
	@GetMapping(value = "/get/img/{codiceFiscale}", produces = "application/json")
	private BaseResponseDto<ImmagineDto> getImgByCodiceFiscale(@PathVariable("codiceFiscale") String codiceFiscale) {
		BaseResponseDto<ImmagineDto> result = new BaseResponseDto<>();
		result.setResponse(componentiService.getImgByCodiceFiscale(codiceFiscale));
		return result;
	}
	
	@GetMapping(value = "/get/{codiceFiscale}", produces = "application/json")
	private BaseResponseDto<ComponentiDto> getByCodiceFiscale(@PathVariable("codiceFiscale") String codiceFiscale) {
		BaseResponseDto<ComponentiDto> result = new BaseResponseDto<>();
		result.setResponse(componentiService.getByCodiceFiscale(codiceFiscale));
		return result;
	}
	
	@PatchMapping(value = "/update/{codiceFiscale}", produces = "application/json")
	private BaseResponseDto<ServiceResponse> update(@PathVariable("codiceFiscale") String codiceFiscale, ComponentiDto dto) {
		BaseResponseDto<ServiceResponse> result = new BaseResponseDto<>();
		ServiceResponse serviceStatus = componentiService.update(codiceFiscale, dto);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	@DeleteMapping(value = "/delete/{codiceFiscale}", produces = "application/json")
	private BaseResponseDto<ServiceResponse> deleteByCodiceFiscale(@PathVariable("codiceFiscale") String codiceFiscale) {
		BaseResponseDto<ServiceResponse> result = new BaseResponseDto<>();
		ServiceResponse serviceStatus = componentiService.deleteByCodiceFiscale(codiceFiscale);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
}
