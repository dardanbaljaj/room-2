package its.gruppo2.upload.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import its.gruppo2.upload.dao.ComponentiDao;

@Repository
@Transactional
public interface ComponentiRepository extends JpaRepository<ComponentiDao, Integer>{

	void deleteByCodiceFiscale(String codiceFiscale);

	ComponentiDao findByCodiceFiscale(String codiceFiscale);

}
