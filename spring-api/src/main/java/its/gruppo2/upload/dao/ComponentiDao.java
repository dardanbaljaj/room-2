package its.gruppo2.upload.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "componenti_gruppo")
@Data
public class ComponentiDao {

	@Id
	@Column(name = "codice_fiscale")
	private String codiceFiscale;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "cognome")
	private String cognome;
	
	@Column(name = "data_di_nascita")
	private Date data_di_nascita;
	
	@Column(name = "luogo_di_nascita")
	private String luogo_di_nascita;
	
	@Column(name = "sesso")
	private String sesso;
	
	@Column(name = "immagine")
	private String immagine;
	
}
