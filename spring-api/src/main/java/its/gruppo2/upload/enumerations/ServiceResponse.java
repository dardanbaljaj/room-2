package its.gruppo2.upload.enumerations;

public enum ServiceResponse {

	OK(1),
	CREATE_FAILED(-1),
	UPDATE_FAILED(-2),
	DELETE_FAILED(-3),
	DATA_INTEGRITY_ERROR(-4),
	EMPTY_DTO(-5),
	EMPTY_CODICE_FISCALE(-6);

    private final int number;

    private ServiceResponse(int number) {
        this.number = number;
    }

    public int value() {
        return number;
    }
	
}
