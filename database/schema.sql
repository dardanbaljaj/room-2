
DROP DATABASE IF EXISTS anagrafica;
CREATE DATABASE IF NOT EXISTS anagrafica;
USE anagrafica;

CREATE TABLE componenti_gruppo (
	codice_fiscale VARCHAR(16) PRIMARY KEY,
	nome VARCHAR(30),
	cognome VARCHAR(30),
	data_di_nascita DATE,
	luogo_di_nascita VARCHAR(30),
	sesso varchar(1),
	immagine VARCHAR(200)
	);
	
INSERT INTO componenti_gruppo VALUES
	('BLJDDN00C11Z148G','Dardan','Baljaj','2000-03-11','Skopje','M',null);
INSERT INTO componenti_gruppo VALUES
	('FRRCST00T60B563Q','Cristina','Ferro','2000-12-20','Camposampiero','F', null);
INSERT INTO componenti_gruppo VALUES
	('MRCMRC00B563D','Marco','Marchioro','2000-02-26','Camposampiero','M', null);
INSERT INTO componenti_gruppo VALUES 
	('FCHMRC00M26A459V','Marco','Fichera','2000-08-26','Arzignano','M', null);
INSERT INTO componenti_gruppo VALUES
	('CSRMRC00P12B563M','Marco','Casarin','2000-09-12','Camposampiero','M', null);
INSERT INTO componenti_gruppo VALUES
	('ZFNMTT100A100X1','Mattia','Zogno','2000-09-10','Abano','M', null);
INSERT INTO componenti_gruppo VALUES
	('PNZMRC00L03B563W','Marco','Panozzo','2000-07-03','Camposampiero','M', null);
INSERT INTO componenti_gruppo VALUES
	('BRTMHL00C22G224C','Michael','Brotto','2000-03-22','Padova','M', null);
